export const roomsMap = new Map();
export const winners = [];

export const getCurrentRoomId = (socket) =>
  Object.keys(socket.rooms).find((roomId) => roomsMap.has(roomId));

export const getRoomArray = () => {
  return Array.from(roomsMap).map(([name, value]) => ({
    roomName: name,
    players: [...value.users],
  }));
};

export const setRoomData = (roomName, players) =>
  roomsMap.set(roomName, {
    users: [...players],
  });

export const setPlayerData = (username, status = 0, progress = 0) => ({
  [username]: {
    status,
    progress,
  },
});

export const playersInfo = (players) =>
  players.map((player) => ({
    [player]: {
      progress: 0,
      status: 0,
    },
  }));

export const markPlayerReady = (players, username, roomId) => {
  players.map((player) => {
    if (player[username]) {
      player[username].status = 1;
    }

    return player;
  });

  return players;
};

export const allowToStartGame = (players) => {
  let isAllowed = true;

  players.forEach((player) => {
    const playerData = player[Object.keys(player)];

    if (parseInt(playerData.status) === 0) {
      isAllowed = false;
    }
  });

  return isAllowed;
};

export const getRandomInt = (min = 0, max = 6) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
};
