import * as wsConfig from "./config";
import { texts } from "../data";
import {
  roomsMap,
  winners,
  playersInfo,
  getRoomArray,
  getCurrentRoomId,
  setPlayerData,
  setRoomData,
  allowToStartGame,
  markPlayerReady,
  getRandomInt,
} from "./helper";

export default (io) => {
  io.on("connection", (socket) => {
    socket.emit("ROOMS_LIST_UPDATE", { rooms: getRoomArray() });

    socket.on("ROOM_CREATED", ({ roomName, username }) => {
      setRoomData(roomName, [setPlayerData(username)]);

      socket.join(roomName, () => {
        io.to(socket.id).emit("JOIN_ROOM_DONE", {
          roomName,
          players: playersInfo([username]),
        });
      });

      socket.broadcast.emit("ROOMS_LIST_UPDATE", { rooms: getRoomArray() });
    });

    socket.on("ROOM_JOIN", ({ roomId, username }) => {
      const prevRoomId = getCurrentRoomId(socket);
      const room = roomsMap.get(roomId);
      if (roomId === prevRoomId) {
        return;
      }
      if (prevRoomId) {
        socket.leave(prevRoomId);
      }

      if (wsConfig.MAXIMUM_USERS_FOR_ONE_ROOM === room.users.length) {
        socket.emit("ROOMS_ALERT", {
          message: `Allowed ${wsConfig.MAXIMUM_USERS_FOR_ONE_ROOM} players only per each room`,
        });
        return;
      }

      socket.join(roomId, () => {
        const players = [...roomsMap.get(roomId).users];

        players.push(setPlayerData(username));

        setRoomData(roomId, players);

        io.to(roomId).emit("JOIN_ROOM_DONE", {
          roomName: roomId,
          players: players,
        });

        socket.broadcast.emit("ROOMS_LIST_UPDATE", { rooms: getRoomArray() });
      });
    });

    socket.on("ROOM_LEAVE", ({ roomId, username }) => {
      const players = [...roomsMap.get(roomId).users];
      players.splice(players.indexOf(username), 1);

      socket.leave(roomId);

      if (players.length <= 0) {
        roomsMap.delete(roomId);
      } else {
        setRoomData(roomId, players);
      }

      socket.broadcast.emit("ROOMS_LIST_UPDATE", { rooms: getRoomArray() });

      io.to(roomId).emit("ROOM_PLAYERS_LIST_UPDATE", {
        players: players,
      });
    });

    socket.on("ROOM_PLAYER_READY", ({ roomId, username }) => {
      const players = [...roomsMap.get(roomId).users];

      markPlayerReady(players, username, roomId);

      io.to(roomId).emit("ROOM_PLAYERS_LIST_UPDATE", { players });

      if (allowToStartGame(players)) {
        const textIndex = getRandomInt();

        io.to(roomId).emit("GAME_START_COUNT_DOWN", {
          timer: wsConfig.SECONDS_TIMER_BEFORE_START_GAME,
          text: texts[textIndex],
          gameTimer: wsConfig.SECONDS_FOR_GAME,
        });
      }
    });

    socket.on("PLAYER_PROGRESS", ({ roomId, username, progress }) => {
      const players = [...roomsMap.get(roomId).users];
      players.forEach((player) => {
        const name = Object.keys(player)[0];
        const playerData = player[username];
        if (name === username) {
          playerData.progress = progress;

          if (progress === 100) {
            winners.push(username);
          }
        }
      });

      setRoomData(roomId, players);
      io.to(roomId).emit("ROOM_PLAYERS_LIST_UPDATE", { players });
    });

    socket.on("GAME_FINISHED", ({ roomId }) => {
      const players = [...roomsMap.get(roomId).users];
      io.to(roomId).emit("GAME_FINISHED_DONE", { winners, players });
    });
  });

  io.on("disconnect", (socket) => {
    console.log(`${socket.id} disconnected`);
  });
};
