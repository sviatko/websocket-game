import { createElement, addClass } from "../helper.mjs";

const username = sessionStorage.getItem("username");
const socket = io("http://localhost:3002/game");

export const playersTotalUpdate = ({ total }) => {
  const totalPlayersElement = document.querySelector("#palyers-total");

  totalPlayersElement.innerText = total;
};

export const playerListRender = ({ players }) => {
  let finishGame = true;
  const listContainer = document.querySelector(".js-players-list");
  listContainer.innerHTML = "";

  Object.keys(players).map((index) => {
    const player = players[index][Object.keys(players[index])[0]];
    const name = Object.keys(players[index])[0];
    const status = player.status;
    const playerItemContainer = createElement({
      tagName: "div",
      className: "game-player",
      attributes: { id: name },
    });
    const playerName = createElement({
      tagName: "div",
      className: "game-player-name ml-5",
    });
    const playerProgressContainer = createElement({
      tagName: "div",
      className: "game-player-progress-bar-container",
    });
    const playerProgressBar = createElement({
      tagName: "div",
      className: "game-player-progress-bar",
    });

    if (player.progress === 100) {
      addClass(playerProgressBar, "game-player-progress-bar-done");
    } else {
      playerProgressBar.style.width = `${player.progress}%`;
      finishGame = false;
    }

    if (parseInt(status) === 1) {
      addClass(playerName, "active");
    }

    if (name === username) {
      addClass(playerName, "you");
    }

    playerName.innerText = name;

    playerProgressContainer.append(playerProgressBar);

    playerItemContainer.append(playerName, playerProgressContainer);
    listContainer.append(playerItemContainer);
  });

  if (finishGame) {
    const roomId = document.querySelector("#room-name").innerText;

    socket.emit("GAME_FINISHED", { roomId });
  }
};
